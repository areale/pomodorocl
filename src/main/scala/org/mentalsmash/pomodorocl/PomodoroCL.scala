package org.mentalsmash.pomodorocl

import scala.concurrent.duration._
import java.applet.Applet
import java.applet.AudioClip

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.text.SimpleDateFormat
import java.io.File
import java.net.URL

object PomodoroCL {
   
  val WorkSliceDuration = Duration(25, MINUTES)
  val ShortBreakDuration = Duration(5, MINUTES)
  val LongBreakDuration = Duration(15, MINUTES) 
  val BeepAudioPath = "/usr/share/sounds/gnome/default/alerts/glass.ogg"
  val AudioPlayerPath = "/usr/bin/mplayer"



  def main(args: Array[String]): Unit = {
    var cmd: Array[String] = Array("/bin/sh", "-c", "stty -echo < /dev/tty")
    Runtime.getRuntime().exec(cmd).waitFor()
    cmd = Array("/bin/sh", "-c", "stty raw < /dev/tty")
    Runtime.getRuntime().exec(cmd).waitFor()
    val exec = Executors.newSingleThreadScheduledExecutor()
    try {
      (new PomodoroCL(exec)).mainLoop
    } finally {
      exec.shutdown()
      cmd = Array("/bin/sh", "-c", "stty cooked < /dev/tty")
      Runtime.getRuntime().exec(cmd).waitFor()
      cmd = Array("/bin/sh", "-c", "stty +echo < /dev/tty")
      Runtime.getRuntime().exec(cmd).waitFor()
    } 
  }
}

final class PomodoroCL(exec: ScheduledExecutorService) extends TimerInterface {

  private val sessionStart = new java.util.Date()
  private var numSessions = 0 
  private var curTimer: Option[Timer] = None
  private val lock = new Object
  private val creader = System.console().reader()
  private val cwriter = System.console().writer()
  var curstatus: Char = '_'
  
  def tick(curValue: Long): Unit = lock.synchronized{
    if (curstatus != '_') {
      printStatus(curValue)
    }
  }
  
  def finished(): Unit = {
    lock.synchronized {
      if (curstatus == 'p') {
        numSessions += 1
      }
      curstatus = '_'
      printStatus()
      playSound()
    }
  }

  private def printStatus(timerValue: Long = 0): Unit = 
    lock.synchronized {
      cwriter.print("\033c")
      cwriter.print("p: start/puse a workslice; b: start/puase a short break;\n\rl: start/puase a long break; c: cancel the current timer;\n\n\r")
      cwriter.print(s"Session started: ${formattedSessionStart}\n\n\r")
      cwriter.print(s"Number of work slices this session: ${numSessions}\n\r")
      cwriter.print(s"Timer: ${timerToString(timerValue)}\n\r")
      cwriter.flush()
    }

 
  private lazy val formattedSessionStart: String = {
    val fmt =  new SimpleDateFormat()
    fmt.format(sessionStart)
  }

  def mainLoop: Unit = {
    printStatus()
    var last = creader.read().asInstanceOf[Char]
    var stop = false
    var timer = -1L
    while(last != 'q') {
      timer = last match {
        case 'p' =>
          PomodoroCL.WorkSliceDuration.toSeconds
        case 'b' =>
          PomodoroCL.ShortBreakDuration.toSeconds
        case 'l' =>
          PomodoroCL.LongBreakDuration.toSeconds
        case 'c' =>
          lock.synchronized {
            curTimer.foreach(c => c.cancel())
            curstatus = '_'
            printStatus()
          }

          -1

        case _ =>
          -1
      }
      if (timer > 0) {
        lock.synchronized {
          val tm = curTimer match {
            case None =>
            val tm = new Timer(exec, timer, 1, this)
            curTimer = Some(tm)
            tm
            case Some(tm)=>
            if ( curstatus != last ) {
              tm.cancel() 
              val newtm = new Timer(exec, timer, 1, this)
              curTimer = Some(newtm)
              newtm 
            } else {
              tm
            }
          }
          curstatus = last
          tm.startStop()
        }
      }
      last = creader.read().asInstanceOf[Char]
    }
    
  }

  private def timerToString(tm: Long): String = {
    require(tm <= 99*60+99)
    val mins = (tm  / 60)
    val secs = (tm % 60)
    return f"$mins%02d:$secs%02d"
  }

  private def playSound(): Unit = {
    val beep: Option[String] = {
      val f = new File(PomodoroCL.BeepAudioPath)
      if (f.exists) Some(f.getAbsolutePath)
      else None
    }
    val player: Option[String] = {
      val f = new File(PomodoroCL.AudioPlayerPath)
      if (f.exists && f.canExecute) Some(f.getAbsolutePath)
      else None
    }
    for ( f <- beep; p <- player) {
      val cmd: Array[String] = Array("/bin/sh", "-c", s"${p} ${f} > /dev/null")
      Runtime.getRuntime().exec(cmd).waitFor()
    }
  }


}


trait TimerInterface {
  def tick(curValue: Long): Unit
  def finished(): Unit
}
