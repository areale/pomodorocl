package org.mentalsmash.pomodorocl
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

class Timer(executor: ScheduledExecutorService, timeout: Long, tick: Long, iface: TimerInterface) {

  private var timeLeft = timeout 
  private val tlock = new Object
  private var task: Option[ScheduledFuture[_]] = None

  def startStop(): Unit = tlock.synchronized{
    task =
      task match {
        case Some(fut) =>
          fut.cancel(false)
          None
        case None => 
          iface.tick(timeLeft)
          Some(executor.scheduleAtFixedRate(new TimerTask, 1, tick, TimeUnit.SECONDS))
      }
  }


  def cancel(): Unit = tlock.synchronized {
      task match {
        case Some(fut) =>
          fut.cancel(false)
        case None => 
      }
      task = None
  }


  final class TimerTask extends Runnable {
    def run() {
      if (timeLeft > tick) {
        timeLeft -= tick
        iface.tick(timeLeft)
      } else if (timeLeft <= tick ) {
        tlock.synchronized {
          timeLeft = 0
          task.foreach(fut => fut.cancel(false))
          task = None
        }
        iface.finished()
      }
    }
    
  }
}
